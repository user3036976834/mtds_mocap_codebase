
"""Sequence-to-sequence model for human motion prediction."""

import numpy as np
import torch
from torch import nn
import torch.nn.functional as F

class Seq2SeqModel(nn.Module):
  """Sequence-to-sequence model for human motion prediction"""

  def __init__(self,
               architecture,
               source_seq_len,
               target_seq_len,
               rnn_size, # hidden recurrent layer size
               num_layers,
               max_gradient_norm,
               batch_size,
               learning_rate,
               learning_rate_decay_factor,
               loss_to_use,
               number_of_actions,
               one_hot=True,
               residual_velocities=False,
               output_dim=67,
               dropout=0.0,
               dtype=torch.float32,
               num_traj=0):
    """Create the model.

    Args:
      architecture: [DEFUNCT]
      source_seq_len: lenght of the input sequence.
      target_seq_len: lenght of the target sequence.
      rnn_size: number of units in the rnn.
      num_layers: number of rnns to stack.
      max_gradient_norm: [DEFUNCT]
      batch_size: the size of the batches used during training;
        the model construction is independent of batch_size, so it can be
        changed after initialization if this is convenient, e.g., for decoding.
      learning_rate: [DEFUNCT] => Now in main file.
      learning_rate_decay_factor: [DEFUNCT] => Now in main file.
      loss_to_use: [DEFUNCT]
      number_of_actions: [DEFUNCT]
      one_hot: [DEFUNCT]
      residual_velocities: [DEFUNCT]
      output_dim: size of observation vector.
      dropout: dropout probability (see model def for layers in which it is used).
      dtype: the data type to use to store internal variables.
      num_traj: [DEFUNCT]
    """
    super(Seq2SeqModel, self).__init__()

    self.HUMAN_SIZE = output_dim
    self.input_size = self.HUMAN_SIZE

    print( "Input size is %d" % self.input_size )

    self.source_seq_len = source_seq_len
    self.target_seq_len = target_seq_len
    self.rnn_size = rnn_size
    self.batch_size = batch_size
    self.dropout = dropout
    self.num_layers = num_layers

    # === Create the RNN that will keep the state ===
    print('rnn_size = {0}'.format( rnn_size ))
    self.cell = torch.nn.GRUCell(self.input_size, self.rnn_size)
    if num_layers > 1:
      self.cell2 = torch.nn.GRUCell(self.rnn_size, self.rnn_size)

    self.fc1 = nn.Linear(self.rnn_size * num_layers, self.HUMAN_SIZE)


  def forward(self, encoder_inputs, decoder_inputs, use_cuda):
    # This appears to be coded in a slightly odd way, but is retained from the
    # pytorch port of XXXXXXXXXXXXXXXX => some confidence it's correct.
    def loop_function(prev, i):
        return prev

    batchsize = encoder_inputs.shape[0]
    encoder_inputs = torch.transpose(encoder_inputs, 0, 1)
    decoder_inputs = torch.transpose(decoder_inputs, 0, 1)

    state = torch.zeros(batchsize, self.rnn_size)
    if self.num_layers > 1:
        state2 = torch.zeros(batchsize, self.rnn_size)
    if use_cuda:
        state = state.cuda()
        if self.num_layers > 1:
            state2 = state2.cuda()

    for i in range(self.source_seq_len-1):
        state = self.cell(encoder_inputs[i], state)
        if self.num_layers > 1:
            state2 = self.cell2(state, state2)
            state2 = F.dropout(state2, self.dropout, training=self.training)

        state = F.dropout(state, self.dropout, training=self.training)

    outputs = []
    prev = None
    for i, inp in enumerate(decoder_inputs):
      if loop_function is not None and prev is not None:
          inp = torch.cat((loop_function(prev, i), inp[:, self.HUMAN_SIZE:]), 1)

      inp = inp.detach()

      state = self.cell(inp, state)
      if self.num_layers > 1:
          state2 = self.cell2(state, state2)
          state_out = torch.cat((state, state2), dim=1)
      else:
          state_out = state

      output = inp[:, 0:self.HUMAN_SIZE] + self.fc1(F.dropout(state_out, self.dropout, training=self.training))

      outputs.append(output.view([1, batchsize, self.HUMAN_SIZE]))
      if loop_function is not None:
        prev = output

    outputs = torch.cat(outputs, 0)
    return torch.transpose(outputs, 0, 1)

  def get_batch(self, data_Y, data_U, actions, stratify=False):
    """Get a random batch of data from the specified bucket, prepare for step.

    Args
      data_Y, data_U: list of sequences truncated at file boundaries or training set defined limits.
      actions: a [DEFUNCT]
      stratify: this is now a misnomer; used at test time to ensure entire test batch is used rather
      than a resampled version.
    Returns
      The tuple (encoder_inputs, decoder_inputs, decoder_outputs);
      the constructed batches have the proper format to call step(...) later.
    """

    # Select entries at random
    probs = np.array([y.shape[0] for y in data_Y])
    probs = probs / probs.sum()
    bsz = self.batch_size if not stratify else len(data_Y)

    if not stratify:
      chosen_keys = np.random.choice(len(data_Y), self.batch_size, p=probs)
      bsz = self.batch_size
    else:
      bsz = len(data_Y)
      chosen_keys = list(range(bsz))

    # How many frames in total do we need?
    total_frames = self.source_seq_len + self.target_seq_len
    traj_size = data_U[1].shape[1]

    encoder_inputs  = np.zeros((bsz, self.source_seq_len-1, self.input_size), dtype=float)
    decoder_inputs  = np.zeros((bsz, self.target_seq_len, self.input_size), dtype=float)
    decoder_outputs = np.zeros((bsz, self.target_seq_len, self.HUMAN_SIZE), dtype=float)

    for i in range( bsz ):

      the_key = chosen_keys[i]

      # Get the number of frames
      n = data_Y[the_key].shape[0]
      if n < total_frames:
        how_did_we_get_here = 1
      assert n >= total_frames, "n of file {:d} too small.".format(the_key)

      # Sample somewherein the middle
      idx = np.random.randint(0, n - total_frames) if n > total_frames else 0

      # Select the data around the sampled points
      data_Y_sel = data_Y[ the_key ][idx:idx + total_frames, :]
      data_U_sel = data_U[ the_key ][idx:idx + total_frames, :]

      # Add the data
      encoder_inputs[i, :, 0:self.HUMAN_SIZE]  = data_Y_sel[0:self.source_seq_len-1, :]
      encoder_inputs[i, :, self.HUMAN_SIZE: ] = data_U_sel[0:self.source_seq_len-1, :]  # <= done
      decoder_inputs[i, :, 0:self.HUMAN_SIZE]  = data_Y_sel[self.source_seq_len-1:self.source_seq_len+self.target_seq_len-1, :]
      decoder_inputs[i, :, self.HUMAN_SIZE: ]  = data_U_sel[self.source_seq_len-1:self.source_seq_len + self.target_seq_len-1, :]
      decoder_outputs[i,:, 0:self.HUMAN_SIZE] = data_Y_sel[self.source_seq_len:self.source_seq_len + self.target_seq_len, :]    # <= done

    return encoder_inputs, decoder_inputs, decoder_outputs


