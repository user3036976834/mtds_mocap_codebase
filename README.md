
## UAI Submission: Customizable Sequence Prediction with Multi-Task Dynamical Systems

This is the codebase for the mocap experiments performed in pytorch; the DHO and double 
pendulum experiments were performed in Julia, which requires some further tidying up and
refactoring. This will be happening regardless of acceptance over the next couple of months.
For the time being, please consider this pytorch code as an example implementation, pending
full release and refactoring of the Julia code.

Data from Ian Mason's paper is available from [https://drive.google.com/open?id=1nqbvzOM_VhlYlotPfsdEgrqQ6vIayHyK](https://drive.google.com/open?id=1nqbvzOM_VhlYlotPfsdEgrqQ6vIayHyK)
which is linked from the [github repo](https://github.com/ianxmason/Fewshot_Learning_of_Homogeneous_Human_Locomotion_Styles).
However, there is some pre-processing with Julia scripts which are not yet available.

In terms of attribution, the code here is substantially adapted from another repository which 
will remain nameless for the purposes of this review to avoid de-anonymization.

## Example usage

#### Training MT-RNN model on all data, as used for style morphing:
```bash
python learn_mtfixbmodel.py --seq_length_out 64 --human_size 64 --input_size 35 --decoder_size 1024 
--decoder_size2 128 --bottleneck 24 --latent_k 3 --batch_size 16 --iterations 20000 --style_ix 9 \
--learning_rate 3e-5 --optimiser Adam --learning_rate_step 1000 --test_every 100 \
--learning_rate_z 0e-4 --learning_rate_mt 8e-4 --hard_em_iters 100 --mt_rnn --no_mt_bias \
--train_set_size -1
```

#### MT-RNN for MTL experiments (Experiment 1)
```bash
python learn_mtfixbmodel.py --seq_length_out 64 --human_size 64 --input_size 35 --decoder_size 1024
--decoder_size2 128 --bottleneck 24 --latent_k 3 --batch_size 16 --iterations 20000 --style_ix 9 \
--learning_rate 3e-5 --optimiser Adam --learning_rate_step 1000 --test_every 100 \
--learning_rate_z 0e-4 --learning_rate_mt 8e-4 --hard_em_iters 100 --mt_rnn --no_mt_bias \
--train_set_size 8
```
The argument `--train_set_size 8` denotes the number of 64-length sequences in the training
set for each style.

#### MT-RNN for LOO experiments (Experiment 2)
```bash
python learn_mtfixbmodel.py --seq_length_out 64 --human_size 64 --input_size 35 --decoder_size 1024 
--decoder_size2 128 --bottleneck 24 --latent_k 3 --batch_size 16 --iterations 20000 --style_ix 1 \
--learning_rate 3e-5 --optimiser Adam --learning_rate_step 1000 --test_every 100 \
--learning_rate_z 0e-4 --learning_rate_mt 8e-4 --hard_em_iters 100 --mt_rnn --no_mt_bias \
--train_set_size -1
```
The argument `--style_ix 1` denotes the style index (1:8) to leave out. More details for all arguments
may be found under `parseopts.py`, or using `python learn_mtfixbmodel.py -h` at the command line.

## Repository Navigation

* **MT-RNN files**: The main file (training loop) is `learn_mtfixbmodel.py`. (You'll also find
`learn_mtmodel.py` which was the original experiments using an inference network for amortized 
inference over z, but was abandoned when the fixed batch / standard VI approach worked better.) 

    Associated with this is the model file. The most important is `mtfixb_model.py`, which
    describes the MT-RNN (see the `MTGRU` class def and `MTModule`). The file `mtfixb_model2.py` defines
    a no-adaptive-bias version (i.e. the dynamical bias $b$ is not adapted) of the MTGRU. This violates
    DRY, and requires refactoring.
    
    When `learn_mtfixbmodel.py` is called it uses the `parseopts.py` file which parses the command line
    options. Default values for all parameters can be found in `default.ini`.

* **Benchmark model files**:  The main file is `run_bmark.py` which is similar in spirit to 
`learn_mtfixbmodel.py`: a training loop over an associated model. The associated model files are 
`seq2seq_model.py` for the open loop GRU, and `seq2seq_model_closed.py` for the closed loop version.

* **'Inference' of z**: Uses the `optim_z.py` file. The AdaIS experiments were performed in Julia, but
as stated in the supp mat, essentially all inference was done via optimization for the mocap experiments.
